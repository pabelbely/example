import React from 'react';
import styled from 'styled-components';

const HeadingH1 = styled.h1`
  color: #123;
  font-size: 32px;
`

const HeadingP = styled.p`
  color: #123;
`

export const Heading = () => {
    return (
        <>
            <HeadingH1>Hello World</HeadingH1>
            <HeadingP>Something</HeadingP>
        </>
    );
}
